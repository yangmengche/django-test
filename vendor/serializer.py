from .models import Vendor
from rest_framework import serializers

class VendorSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Vendor
        fields = ['vendor_name', 'store_name', 'phone', 'address', 'note']