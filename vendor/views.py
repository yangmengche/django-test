from django.shortcuts import render,  get_object_or_404
from .models import Vendor
from .form import VendorForm, RawVendorForm
# from django.http import Http404
from django.views.generic import ListView, DetailView, CreateView
from rest_framework import viewsets, permissions
from .serializer import VendorSerializer

# Create your views here.
def vendor_index(request):
    vendor_list = Vendor.objects.all()
    context = {'vendor_list': vendor_list}
    return render(request, 'vendor/list.html', context)

def vendor_create_view(request):
    form = VendorForm(request.POST or None)
    if form.is_valid():
        form.save()
        form=VendorForm()

    context = {
        'form':form
    }
    return render(request, "vendor/create.html", context)

def vendor_create_raw(request):
    form = RawVendorForm(request.POST or None)
    if form.is_valid():
        print(form.cleaned_data)
        form=VendorForm()

    context = {
        'form':form
    }
    return render(request, "vendor/create.html", context)

def singleVendor(request, id):
    # try:
    #     obj = Vendor.objects.get(id=id)
    # except Vendor.DoesNotExist:
    #     raise Http404
    obj = get_object_or_404(Vendor, id=id)
    context= {
        'vendor': obj
    }
    return render(request, 'vendor/info.html', context)


class VendorListView(ListView):
    model = Vendor
    template_name = 'vendor/list2.html'
    
class VendorDetailView(DetailView):
    model = Vendor
    template_name = 'vendor/info.html'
    # def get_object(self, queryset=None):
    #     return Vendor.objects.get(id=self.kwargs.get('id'))

class VendorCreateView(CreateView):
    model = Vendor
    fields = '__all__'
    template_name = 'vendor/create.html'
    

class VendorViewSet(viewsets.ModelViewSet):
    queryset = Vendor.objects.all()
    serializer_class = VendorSerializer
    permission_classes = [permissions.IsAuthenticated]