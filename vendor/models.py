from django.db import models
from django.contrib import admin
from django.utils.translation import gettext_lazy as _
from django.urls import reverse

# Create your models here.
class Vendor(models.Model):
    vendor_name = models.CharField(max_length=20)
    store_name = models.CharField(max_length=20)
    phone = models.CharField(max_length=10)
    address = models.CharField(max_length=100)
    note = models.CharField(max_length=100)

    def __str__(self):
        return self.vendor_name

    def get_absolute_url(self):
        return reverse("vendor:vendor2", kwargs={"pk":self.id})
        # return f"/vendor/{self.id}"

@admin.register(Vendor)
class VendorAdmin(admin.ModelAdmin):
    list_display = ['id', 'vendor_name', 'store_name', 'phone', 'address', 'note']
    # list_display = [field.name for field in Vendor._meta.fields]  //not work

class Food(models.Model):
    food_name = models.CharField(max_length=30)
    price = models.DecimalField(max_digits=3, decimal_places=0)
    food_vendor = models.ForeignKey(Vendor, on_delete=models.CASCADE)
    def __str__(self):
        return self.food_name

class PriceFilter(admin.SimpleListFilter):
    title = _('price')
    parameter_name = 'compareprice'

    def lookups(self, request, model_admin):
        return (
            ('>50', _('>50')),
            ('<=50', _('<=50'))
        )
    def queryset(self, request, queryset):
        if self.value() == '>50':
            return queryset.filter(price__gt=50)
        if self.value() == '<=50':
            return queryset.filter(price__lte=50)


@admin.register(Food)
class FoodAdmin(admin.ModelAdmin):
    list_display = ['id', 'food_name', 'price', 'food_vendor']
    list_filter = ('price', PriceFilter)
    search_fields = ('food_name', 'food_vendor')

