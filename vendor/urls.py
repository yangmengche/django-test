from django.urls import path, include
from . import views
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'vendors', views.VendorViewSet, basename='vendor')


app_name='vendor'
urlpatterns = [
    path('list', views.vendor_index, name="vendor_index"),
    path('<int:id>/', views.singleVendor, name='vendor'),
    path('create', views.vendor_create_view, name="vendor_create_view"),
    path('createraw', views.vendor_create_raw, name="vendor_create_raw"),
    path('', views.VendorListView.as_view(), name='vendor_index2'),
    path('detail/<int:pk>/', views.VendorDetailView.as_view(), name='vendor2'),
    path('create2', views.VendorCreateView.as_view(), name='vendor_create_view2'),
    path('restful/', include(router.urls)),
]